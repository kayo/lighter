#include <libopencm3/cm3/nvic.h>

#ifndef NDEBUG

#ifndef STM32F0
void memory_manage_fault_handler(void) {
  for(; 1; );
}

void bus_fault_handler(void) {
  for(; 1; );
}

void usage_fault_handler(void) {
  for(; 1; );
}
#endif

void hard_fault_handler(void) {
  for(; 1; );
}

#endif /* <NDEBUG */
