libsystem.BASEPATH := $(dir $(lastword $(MAKEFILE_LIST)))

TARGET.LIBS += libsystem
libsystem.INHERIT := firmware libopencm3
libsystem.CDIRS := $(libsystem.BASEPATH)include
libsystem.modules += systick memtool fault
libsystem.SRCS := $(foreach m,$(libsystem.modules),$(libsystem.BASEPATH)src/$(word 1,$(subst :, ,$(m))).c$(if $(word 2,$(subst :, ,$(m))),?instance=$(word 2,$(subst :, ,$(m)))))
