#include "macro.h"

#define console_fn(N, F) _CAT4(console, N, _, F)

#define console_def(N)            \
  void console_fn(N, init)(void); \
  void console_fn(N, done)(void); \
  void console_fn(N, step)(const param_coll_t *params);
