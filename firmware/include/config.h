/* common config */
#define mcu_frequency 48
#define systick_config 1,100

#define sensor_adc 1,12,1.2,3.3 /* adc device,adc bits,adc Vref,adc Vtop */
#define sensor_dma 1,1 /* device,channel */
#define motion_input A,5,5 /* port,pad,channel */
#define therm_input A,6,6 /* port,pad,channel */
#define photo_input A,7,7 /* port,pad,channel */

//#define filter_config 16,8 /* average samples,median samples */
#define filter_config 24,8 /* average samples,median samples */

#define motion_config 0,(0.1,2.0,3.3) /* fraction bits,(min value,middle value,max value) */
//#define therm_config 4,(4.7e3,0,HI),SH,(0.0013081992342927878,0.00005479611105636211,9.415113523239857e-7) /* fraction bits,resistors(r1,r2,r place),model,params(a,b,c) */
#define therm_config 0,(22e3,0,LO),BETA,(3450,100e3,C2K(24)) /* fraction bits,resistors(r1,r2,r place),model,params(beta,r0,t0) */
#define photo_config 0,(10e3,0,LO),LDR,(0.7,14e3,10) /* fraction bits,resistors(r1,r2,r place),model,params(gamma,r0,l0) */

#define vref_config 10 /* fraction bits */
#define tmcu_config 4,30,1.43,4.3 /* fraction bits,T0,Vsense(T0),Avg Slope */

#define light_output B,1,0 /* port,pad,active level */

/**
 * Console config
 */
//#define console_gpio (A,10,1),(A,9,1),rs485,(A,1,1) /* rx(port,pad,af),tx(port,pad,af),rs485,de(port,pad,af) */
#define console_gpio (A,10,1),(A,9,1),none /* rx(port,pad,af),tx(port,pad,af),none */
#define console_uart 1,115200,8,1 /* uart,baudrate,databits,stopbits */
#define console_dma 1,(3,512),(2,512) /* dma,rx(channel,buffer length),tx(channel,buffer length) */
#define console_proto json /* json|bin */

/**
 * Radio interface control
 */
#define radio_enable A,0,1 /* port,pad,active level */
 
/**
 * Compatibility with STM32F0
 */

#ifdef NVIC_DMA1_CHANNEL2_3_IRQ
#define dma1_channel2_isr dma1_channel2_3_isr
#define dma1_channel3_isr dma1_channel2_3_isr
#define NVIC_DMA1_CHANNEL2_IRQ NVIC_DMA1_CHANNEL2_3_IRQ
#define NVIC_DMA1_CHANNEL3_IRQ NVIC_DMA1_CHANNEL2_3_IRQ
#endif

#ifdef NVIC_DMA1_CHANNEL4_5_IRQ
#define dma1_channel4_isr dma1_channel4_5_isr
#define dma1_channel5_isr dma1_channel4_5_isr
#define NVIC_DMA1_CHANNEL4_IRQ NVIC_DMA1_CHANNEL4_5_IRQ
#define NVIC_DMA1_CHANNEL5_IRQ NVIC_DMA1_CHANNEL4_5_IRQ
#endif
