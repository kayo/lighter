#ifndef __SENSOR_H__
#define __SENSOR_H__ "sensor.h"

void sensor_init(void);
void sensor_done(void);

void sensor_resume(void);

int16_t sensor_therm(void);
int16_t sensor_photo(void);
int16_t sensor_motion(void);

#endif /* __SENSOR_H__ */
