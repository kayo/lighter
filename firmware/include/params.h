#ifndef __PARAMS_H__
#define __PARAMS_H__

#include <stdbool.h>

#include PARAMS_params_H

typedef enum {
  mode_off,
  mode_auto,
  mode_manual,
} mode_t;

#endif
