#ifndef __DRIVER_H__
#define __DRIVER_H__

#include <stdbool.h>

void driver_init(void);
void driver_done(void);

void driver_light(bool val);

#endif /* __DRIVER_H__ */
