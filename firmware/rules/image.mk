OCD_INTERFACE ?= jlink
OCD_TRANSPORT ?= swd
OCD_TARGET    ?= stm32f1x

OCD_COMMAND = $(Q)openocd \
  -c "source [find interface/$(OCD_INTERFACE).cfg]" \
  $(if $(OCD_TRANSPORT),-c "transport select $(OCD_TRANSPORT)") \
  -c "source [find target/$(OCD_TARGET).cfg]"

IMG_P = bin/$(1).bin

define IMG_RULES
$$(eval $$(call BIN_RULES,$(1)))

$(1).IMG := $$(call IMG_P,$(1))
build: build.img.$(1)
build.img.$(1): $$($(1).IMG)
$$($(1).IMG): $$($(1).BIN)
	@echo TARGET $(1) IMAGE
	$(Q)$(OBJCOPY) -O binary $$< $$@

clean: clean.img.$(1)
clean.img.$(1):
	$(Q)rm -f $$($(1).IMG)

flash.img.$(1): $$($(1).BIN)
	$(OCD_COMMAND) -c "init" -c "program $$< verify reset" -c "shutdown"
endef

options:
	$(OCD_COMMAND) -c "init" -c "reset" -c "halt" -c "$(OCD_TARGET) options_read 0" -c "shutdown"

protect:
	$(OCD_COMMAND) -c "init" -c "halt" -c "flash protect 0 0 last on" -c "reset run" -c "shutdown"

unprotect:
	$(OCD_COMMAND) -c "init" -c "halt" -c "flash protect 0 0 last off" -c "reset run" -c "shutdown"

restart:
	$(OCD_COMMAND) -c "init" -c "reset init" -c "reset run" -c "shutdown"

debug:
	$(OCD_COMMAND) -c "init" -c "reset init"

start.debug:
	$(OCD_COMMAND) -c "init" -c "reset init" &

stop.debug:
	$(Q)killall openocd

attach:
	$(OCD_COMMAND)
