cmdebug.BASEPATH := $(subst $(dir $(abspath $(CURDIR)/xyz)),,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))

cmdebug.GDBOPTS := \
  -ex 'source $(cmdebug.BASEPATH)cmdebug/gdb.py' \
  -ex 'svd_load $(cmdebug.BASEPATH)cmdebug/$(cmdebug.platform).svd'
