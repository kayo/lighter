#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/exti.h>

#include "macro.h"
#include "systick.h"
#include "monitor.h"
#include "config.h"

#if USE_MEMCHECK
#include "memtool.h"
#endif

#if USE_PARAMS
#include "params.h"
#endif

#if USE_SENSOR
#include "sensor.h"
#endif

#include "driver.h"
#include "console.h"

#if USE_RADIO
#define RADIO_ENABLE_PORT _CAT2(GPIO, _NTH0(radio_enable))
#define RADIO_ENABLE_PAD _CAT2(GPIO, _NTH1(radio_enable))
#define RADIO_ENABLE_LEVEL _NTH2(radio_enable)

#if RADIO_ENABLE_LEVEL
#define radio_enable_off gpio_clear
#define radio_enable_on gpio_set
#else /* !RADIO_ENABLE_LEVEL */
#define radio_enable_off gpio_set
#define radio_enable_on gpio_clear
#endif /* <RADIO_ENABLE_LEVEL */

static void radio_on(void) {
	radio_enable_on(RADIO_ENABLE_PORT, RADIO_ENABLE_PAD);
}

static void radio_off(void) {
  radio_enable_off(RADIO_ENABLE_PORT, RADIO_ENABLE_PAD);
}

static void radio_init(void) {
  /* Turn off radio */
  radio_off();
  
#ifdef STM32F1
  gpio_set_mode(RADIO_ENABLE_PORT,
                GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_PUSHPULL,
                RADIO_ENABLE_PAD);
#else /* !STM32F1 */
  gpio_mode_setup(RADIO_ENABLE_PORT,
                  GPIO_MODE_OUTPUT,
                  GPIO_PUPD_NONE,
                  RADIO_ENABLE_PAD);
  gpio_set_output_options(RADIO_ENABLE_PORT,
                          GPIO_OTYPE_PP,
                          GPIO_OSPEED_HIGH,
                          RADIO_ENABLE_PAD);
#endif /* <STM32F1 */
}
#endif /* <USE_RADIO */

monitor_def();
console_def();

static inline void hardware_init(void) {
  /* Enable GPIO clock */
  rcc_periph_clock_enable(RCC_GPIOA);
  rcc_periph_clock_enable(RCC_GPIOB);
  
  /* Enable DMA1 clock */
  rcc_periph_clock_enable(RCC_DMA); 
}

static inline void hardware_done(void) {
  /* Disable DMA1 clock */
  rcc_periph_clock_disable(RCC_DMA);

  /* Disable GPIO clock */
  rcc_periph_clock_disable(RCC_GPIOA);
  rcc_periph_clock_disable(RCC_GPIOB);
}

static inline void init(void) { 
  rcc_clock_setup_in_hsi_out_48mhz();
  
  hardware_init();
  systick_init();
  
#if USE_SENSOR
  sensor_init();
#endif
  
  driver_init();

#if USE_CONSOLE
  console_init();
#endif

#if USE_RADIO
  radio_init();
  radio_on();
#endif
}

#if 0
static inline void done(void) {
#if USE_CONSOLE
  console_done();
#endif
  
  driver_done();
  
#if USE_SENSOR
  sensor_done();
#endif

  systick_done();
  hardware_done();
}
#else
#define done()
#endif

#if USE_MEMCHECK
static mem_info_t mem_info;
#endif

static uint16_t mS = 0;

int main(void) {
#if USE_MEMCHECK
  mem_prefill();
#endif
  
  init();

#if USE_PARAMS
  param_init(&params);
#endif
  
  monitor_init();
  
  for (;;) {
#if USE_SENSOR
    /* Trigger sensor */
    sensor_resume();
#endif
    
    monitor_wait();

    mS += _NTH1(systick_config);

    if (mS >= 1000) {
      mS -= 1000;

      if (time > 0) {
        time --;
      }
    }

    motion = sensor_motion();
    therm = sensor_therm();
    photo = sensor_photo();

    switch (mode) {
    case mode_off:
      light = false;
      break;
    case mode_auto:
      if (photo < photo_thres &&
          (motion > motion_thres ||
           motion < -motion_thres)) {
        time = auto_time;
      }
      light = time > 0;
      break;
    case mode_manual:
      break;
    }

    driver_light(light);

    console_step(&params);

#if USE_MEMCHECK
    mem_measure(&mem_info);
#endif
  }
  
  done();
  
  return 0;
}
