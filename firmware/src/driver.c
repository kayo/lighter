#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>

#include "driver.h"
#include "macro.h"
#include "config.h"

#define CTL_PORT _CAT2(GPIO, _NTH0(light_output))
#define CTL_PAD _CAT2(GPIO, _NTH1(light_output))
#define CTL_ACT _NTH2(light_output)

#if CTL_ACT
#define ctl_off gpio_clear
#define ctl_on gpio_set
#else /* !CTL_ACT */
#define ctl_off gpio_set
#define ctl_on gpio_clear
#endif /* <CTL_ACT */

void driver_init(void) {
  /* Turn off pump */
  driver_light(false);
  
#ifdef STM32F1
  gpio_set_mode(CTL_PORT,
                GPIO_MODE_OUTPUT_50_MHZ,
				GPIO_CNF_OUTPUT_OPENDRAIN,
                CTL_PAD);
#else /* !STM32F1 */
  gpio_mode_setup(CTL_PORT,
				  GPIO_MODE_OUTPUT,
				  GPIO_PUPD_NONE, CTL_PAD);
  gpio_set_output_options(CTL_PORT, GPIO_OTYPE_OD,
						  GPIO_OSPEED_HIGH, CTL_PAD);
#endif /* <STM32F1 */
}

void driver_done(void) {
  
}

static bool light_val = true;

void driver_light(bool val) {
  if (val != light_val) {
	light_val = val;
	if (val) {
	  ctl_on(CTL_PORT, CTL_PAD);
	} else {
	  ctl_off(CTL_PORT, CTL_PAD);
	}
  }
}
