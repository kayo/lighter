#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/crc.h>
#include <libopencm3/cm3/nvic.h>

#include <stdlib.h>

#include "macro.h"
#include "config.h"

#include "console.h"
#include "param.h"
#include "param-iface.h"
#include "param-json.h"

#ifdef instance
#define CONSOLE_NUM instance
#else
#define CONSOLE_NUM
#endif

#ifndef CONSOLE_DEBUG
#define CONSOLE_DEBUG 0
#endif

console_def(CONSOLE_NUM);

#define UART_CONF _CAT3(console, CONSOLE_NUM, _uart)
#define DMA_CONF _CAT3(console, CONSOLE_NUM, _dma)

#define UART_NUM _NTH0(UART_CONF)
#define UART_RE _CAT3(uart, UART_NUM, _remap)

#define UART_RCC _CAT2(RCC_USART, UART_NUM)
#define UART_DEV _CAT2(USART, UART_NUM)
#define BAUDRATE _NTH1(UART_CONF)
#define DATABITS _NTH2(UART_CONF)
#define STOPBITS _CAT2(USART_STOPBITS_, _NTH3(UART_CONF))

#define DMA_NUM _NTH0(DMA_CONF)

#define DMA_DEV _CAT2(DMA, DMA_NUM)
#define DMA_RX_CONF _UNWR(_NTH1(DMA_CONF))
#define DMA_TX_CONF _UNWR(_NTH2(DMA_CONF))

#define DMA_RX_NUM _NTH0(DMA_RX_CONF)
#define DMA_RX_LEN _NTH1(DMA_RX_CONF)
#define DMA_RX_CH _CAT2(DMA_CHANNEL, DMA_RX_NUM)
#define DMA_RX_IRQ _CAT5(NVIC_DMA, DMA_NUM, _CHANNEL, DMA_RX_NUM, _IRQ)
#define dma_rx_isr _CAT5(dma, DMA_NUM, _channel, DMA_RX_NUM, _isr)

#define DMA_TX_NUM _NTH0(DMA_TX_CONF)
#define DMA_TX_LEN _NTH1(DMA_TX_CONF)
#define DMA_TX_CH _CAT2(DMA_CHANNEL, DMA_TX_NUM)
#define DMA_TX_IRQ _CAT5(NVIC_DMA, DMA_NUM, _CHANNEL, DMA_TX_NUM, _IRQ)
#define dma_tx_isr _CAT5(dma, DMA_NUM, _channel, DMA_TX_NUM, _isr)

#define PROTO_bin 0
#define PROTO_json 1

#define IO_PROTO _CAT2(PROTO_, _NTH0(console_proto))

#ifdef STM32F1
#define TX_PORT _CAT4(GPIO_BANK_USART, UART_NUM, UART_RE, _TX)
#define TX_PAD _CAT4(GPIO_USART, UART_NUM, UART_RE, _TX)
#define RX_PORT _CAT4(GPIO_BANK_USART, UART_NUM, UART_RE, _RX)
#define RX_PAD _CAT4(GPIO_USART, UART_NUM, UART_RE, _RX)
  
#define USART_TDR USART_DR
#define USART_RDR USART_DR

#define usart_clear_flag(usart, flag)
#else /* !STM32F1 */

#define RX_GPIO _UNWR(_NTH0(console_gpio))
#define RX_PORT _CAT2(GPIO, _NTH0(RX_GPIO))
#define RX_PAD _CAT2(GPIO, _NTH1(RX_GPIO))
#define RX_AF _CAT2(GPIO_AF, _NTH2(RX_GPIO))

#define TX_GPIO _UNWR(_NTH1(console_gpio))
#define TX_PORT _CAT2(GPIO, _NTH0(TX_GPIO))
#define TX_PAD _CAT2(GPIO, _NTH1(TX_GPIO))
#define TX_AF _CAT2(GPIO_AF, _NTH2(TX_GPIO))

#define MODE_none 0
#define MODE_rs485 1

#define RS485 _CAT2(MODE_, _NTH2(console_gpio))

#if RS485
#define DE_GPIO _UNWR(_NTH3(console_gpio))
#define DE_PORT _CAT2(GPIO, _NTH0(DE_GPIO))
#define DE_PAD _CAT2(GPIO, _NTH1(DE_GPIO))
#define DE_AF _CAT2(GPIO_AF, _NTH2(DE_GPIO))
#endif

#define USART_STOPBITS_1 USART_CR2_STOP_1_0BIT
#define USART_SR_IDLE USART_ISR_IDLE
#define USART_SR_NE USART_ISR_NF
#define USART_SR_FE USART_ISR_FE
#define USART_SR_ORE USART_ISR_ORE

static void usart_clear_flag(uint32_t usart, uint32_t flag) {
  USART_ICR(usart) |= flag;
}
#endif /* <STM32F1 */

static union {
  uint8_t rx[DMA_RX_LEN];
  uint8_t tx[DMA_TX_LEN];
} io;

static void tx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
}

static void tx_enable(uint16_t size) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_TX_CH, size);
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_TX_CH);
}

static void rx_disable(void) {
  /* Stop RX DMA transfer */
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
}

static void rx_enable(void) {
  /* Set max number of data bytes */
  dma_set_number_of_data(DMA_DEV, DMA_RX_CH, sizeof(io.rx));
  
  /* Start RX DMA transfer */
  dma_enable_channel(DMA_DEV, DMA_RX_CH);
}

void console_fn(CONSOLE_NUM, init)(void) {
  /* Enable USART clock */
  rcc_periph_clock_enable(UART_RCC);

#if IO_PROTO == PROTO_bin
  /* Enable CRC unit clock */
  rcc_periph_clock_enable(RCC_CRC);
#endif

  /* Enable DMA_DEV TX channel interrupt */
  nvic_enable_irq(DMA_TX_IRQ);

#ifdef STM32F1
  /* Setup GPIO pin TX for transmit */
	gpio_set_mode(TX_PORT, GPIO_MODE_OUTPUT_50_MHZ,
                GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, TX_PAD);
  /* Setup GPIO pin RX for receive */
	gpio_set_mode(RX_PORT, GPIO_MODE_INPUT,
                GPIO_CNF_INPUT_FLOAT, RX_PAD);
#else /* !STM32F1 */
  gpio_mode_setup(TX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, TX_PAD);
  gpio_set_af(TX_PORT, TX_AF, TX_PAD);
  gpio_set_output_options(TX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, TX_PAD);

#if RS485
  gpio_mode_setup(DE_PORT, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, DE_PAD);
  gpio_set_af(DE_PORT, DE_AF, DE_PAD);
  gpio_set_output_options(DE_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, DE_PAD);
#endif
  
  gpio_mode_setup(RX_PORT, GPIO_MODE_AF, GPIO_PUPD_NONE, RX_PAD);
  gpio_set_af(RX_PORT, RX_AF, RX_PAD);
  gpio_set_output_options(RX_PORT, GPIO_OTYPE_PP, GPIO_OSPEED_HIGH, RX_PAD);
#endif /* <STM32F1 */
  
  /* Setup USART parameters */
	usart_set_baudrate(UART_DEV, BAUDRATE);
	usart_set_databits(UART_DEV, DATABITS);
	usart_set_stopbits(UART_DEV, STOPBITS);
	usart_set_parity(UART_DEV, USART_PARITY_NONE);
	usart_set_flow_control(UART_DEV, USART_FLOWCONTROL_NONE);
  usart_set_mode(UART_DEV, USART_MODE_TX_RX);

  /* rs485 */
#if RS485
#ifndef STM32F1
  USART_CR3(UART_DEV) &= ~(USART_CR3_DEP); // | USART_CR3_OVRDIS | USART_CR3_ONEBIT);
  USART_CR3(UART_DEV) |= USART_CR3_DEM | USART_CR3_DDRE;
  //USART_CR2(UART_DEV) |= USART_CR2_TXINV | USART_CR2_RXINV;
#endif
#endif

  /* Setup USART DMA transfer */
  
  /* TX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_TX_CH);
  dma_set_priority(DMA_DEV, DMA_TX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_TX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_TX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_TX_CH);
  dma_set_read_from_memory(DMA_DEV, DMA_TX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_TX_CH, (uint32_t)&USART_TDR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_TX_CH, (uint32_t)io.tx);
  
  /* RX transfer DMA */
  dma_channel_reset(DMA_DEV, DMA_RX_CH);
  dma_set_priority(DMA_DEV, DMA_RX_CH, DMA_CCR_PL_MEDIUM);
  dma_set_peripheral_size(DMA_DEV, DMA_RX_CH, DMA_CCR_PSIZE_8BIT);
  dma_set_memory_size(DMA_DEV, DMA_RX_CH, DMA_CCR_MSIZE_8BIT);
  dma_enable_memory_increment_mode(DMA_DEV, DMA_RX_CH);
  dma_enable_circular_mode(DMA_DEV, DMA_RX_CH);
  dma_set_read_from_peripheral(DMA_DEV, DMA_RX_CH);
  dma_set_peripheral_address(DMA_DEV, DMA_RX_CH, (uint32_t)&USART_RDR(UART_DEV));
  dma_set_memory_address(DMA_DEV, DMA_RX_CH, (uint32_t)io.rx);
  
  /* Enable TX transfer complete interrupt */
  dma_enable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);

  /* Enable TX transfer error interrupt */
  /*dma_enable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  
  usart_enable_tx_dma(UART_DEV);
  usart_enable_rx_dma(UART_DEV);
  
	/* Enable the USART */
	usart_enable(UART_DEV);
  rx_enable();

  //usart_clear_flag(UART_DEV,
  //                 USART_ICR_IDLECF);
}

void console_fn(CONSOLE_NUM, done)(void) {
  /* Disable the USART */
	usart_disable(UART_DEV);
  
  usart_disable_tx_dma(UART_DEV);
  usart_disable_rx_dma(UART_DEV);
  
  /* Disable the DMA_DEV channel4 and channel5 */
  dma_disable_transfer_complete_interrupt(DMA_DEV, DMA_TX_CH);
  /*dma_disable_transfer_error_interrupt(DMA_DEV, DMA_TX_CH);*/
  dma_disable_channel(DMA_DEV, DMA_TX_CH);
  dma_disable_channel(DMA_DEV, DMA_RX_CH);
  
  /* Disable DMA_DEV channel4 interrupt */
  nvic_disable_irq(DMA_TX_IRQ);

#if IO_PROTO == PROTO_bin
  /* Disable CRC unit clock */
  rcc_periph_clock_disable(RCC_CRC);
#endif

  /* Disable USART clock */
  rcc_periph_clock_disable(UART_RCC);
}

void dma_tx_isr(void) {
  /*bool error = dma_get_interrupt_flag(DMA_DEV, DMA_TX_CH, DMA_TEIF);*/
  if (dma_tx_isr != dma_rx_isr ||
      dma_get_interrupt_flag(DMA_DEV, DMA_TX_CH, DMA_TCIF)) {
    dma_clear_interrupt_flags(DMA_DEV, DMA_TX_CH,
                              DMA_GIF |
                              DMA_TEIF |
                              DMA_TCIF);
    tx_disable();
    rx_enable();
  }
}

static inline uint32_t dma_is_active(uint32_t dma, uint8_t channel) {
  return DMA_CCR(dma, channel) & DMA_CCR_EN;
}

static inline uint32_t dma_count_data(uint32_t dma, uint8_t channel) {
  return DMA_CNDTR(dma, channel);
}

#if IO_PROTO == PROTO_bin
static inline uint32_t rbit32(uint32_t val) {
  asm volatile("rbit %0,%0":"+r" (val):"r" (val));
  return val;
}

static uint32_t crc_update(const uint8_t *ptr, uint16_t len) {
  //  uint32_t l = len / 4, e = len & 3;
  const uint32_t *buf = (const uint32_t*)ptr;
  uint32_t crc = 0, crc_ = CRC_DR;
  
  for (; len >= 4; len -= 4)
    crc_ = crc_calculate(rbit32(*buf++));
  
  crc = rbit32(crc_);
  
  if (len > 0) {
    uint8_t len8 = len << 3;
    
    /* reset crc data register (=0) */
    crc_calculate(crc_);
    
    crc = (crc >> len8) ^ rbit32( crc_calculate( rbit32( (*buf & ((1 << len8) - 1)) ^ crc ) >> (32 - len8) ) );
  }
  
  return ~crc;
}
#endif

void console_fn(CONSOLE_NUM, step)(const param_coll_t *params) {
  if (!dma_is_active(DMA_DEV, DMA_RX_CH) ||
      !usart_get_flag(UART_DEV,
                      USART_SR_IDLE)) {
    return;
  }
  
  usart_clear_flag(UART_DEV,
                   USART_ICR_IDLECF);

  uint16_t length;
  
  if (usart_get_flag(UART_DEV,
                     USART_SR_NE |
                     USART_SR_FE |
                     USART_SR_ORE)) {
#ifdef STM32F1
    (void)usart_recv(UART_DEV);
#else
    usart_clear_flag(UART_DEV,
                     USART_ICR_NCF |
                     USART_ICR_FECF |
                     USART_ICR_ORECF);
#endif
    length = 0;
    goto resume;
  }

  length = sizeof(io.rx) - dma_count_data(DMA_DEV, DMA_RX_CH);
  
#if IO_PROTO == PROTO_json
  /* json text interface */
  if (length > 0) {
#if CONSOLE_DEBUG
    const size_t extra = length;
#else
    const size_t extra = 0;
#endif
    int res = param_json_handle(params, (const char*)io.rx, (const char*)io.rx + length, (char*)io.tx + extra, (char*)io.tx + sizeof(io.tx));
    if (res == 0) { /* partial content */
      return;
    }
    length = (res > 0 ? res : 0) + extra;
  }
#endif /* IO_PROTO == PROTO_json */

#if IO_PROTO == PROTO_bin
  /* binary interface */
  const param_size_t *ilen = (const param_size_t *)io.rx;
  
  if (length < sizeof(*ilen)) {
    /* partial content */
    return;
  }
  
  const param_cell_t *iptr = io.rx + sizeof(*ilen);
  const uint32_t *icrc = (const uint32_t*)iptr + *ilen;
  
  param_size_t *olen = (param_size_t *)io.tx;
  param_cell_t *optr = io.tx + sizeof(*olen);
  
  *olen = sizeof(io.tx);
  
  if (*ilen < sizeof(io.tx) - sizeof(*ilen) - sizeof(*icrc)) {
    if (length < sizeof(*ilen) + *ilen + sizeof(*icrc)) {
      /* partial content */
      return;
    }
    
    crc_reset();
    
    if (crc_update(iptr, *ilen) == *icrc) {
      param_iface_handle(params, iptr, *ilen, optr, olen);
    } else {
      *olen = 0;
    }
  } else {
    *olen = 0;
  }
  
  uint32_t *ocrc = (uint32_t*)optr + *olen;
  
  crc_reset();
  *ocrc = crc_update(optr, *olen);
  
  length = sizeof(*olen) + *olen + sizeof(*ocrc);
#endif /* IO_PROTO == PROTO_bin */

 resume:
  rx_disable();
  
  if (length > 0) {
    tx_enable(length);
  } else {
    rx_enable();
  }
}
